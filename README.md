Lambda Cross Account S3 File Upload

Attach the policy to Source S3 Bucket of the Source Account

The Bucket policy set up in the source AWS account. Do NOT forget to change the account number and bucket name in the below policy.

Bucket Policy for S3-Bucket “Source Account”

Create a Lambda function:-
Give A Name To The Lambda Function.
Head-On to IAM.
Create A Role For Lambda Function.
Attach Policies to the respective role.

Policies for Lambda Function:-

         1. S3 Put-Access to Lambda Role in Source Account

         2. Create An Inline Policy For the Lambda Role for allowing to Upload Objects to Destination Account S3 Bucket.

Now Edit the Trust-Relationship of the respective role in Source Account and attach a Assume-Role-Policy.

Head On to the Destination Account:-

1. Create A role as:-

Select type of trusted entity as:-
1.Another AWS Account 2. Provide Your Root A/c Number in the Desired Checkbox

2. Attach Policy to it

Policies:-

           1. S3 Put Permissions:-
           2. Edit the trust-relationship of Destination Account and provide the Account Number Of Source Account

CREATE A SSM SESSION FOR (windows) Machine:-

            1. Remove all the inbound rules from the target ec2 instance (windows).

            2. Login to ec2 instance using ssm and create the user

Commands to create User:-

            1.  $Password = Read-Host -AsSecureString
                 Provide Your Password

            2.  New-LocalUser “<<Name-Of-User-You-Want-To-Provide>>” -Password $Password

            3.  Add-LocalGroupMember “Administrators” -Member “<<Name-of-the-User-Just-Created>>”

Now Create a IAM user in the Source Account and attach the Following Policy:-

            1. Attach Policy for SSM
            2. S3 Read Only Access
            3. S3 Put Access

Now create a role with "AWSEC2SSM" Policy and attach the role to the target instance(windows).

Now install the session manager plugin to the system.

Refer this Link :-

                     (https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-working-with-install-plugin.html#install-plugin-linux)

Configure the aws-cli with the above created IAM user.

Now to start session use following command:-

                  aws ssm start-session --target <<instance-id>> --document-name AWS-StartPortForwardingSession --parameters "localPortNumber=54321,portNumber=3389" --region us-east-1 --no-verify-ssl

Linux Machine Configuration

S3 Download Script as a cron-job in linux machine:-

Note

                  The Linux Machine Should have S3Read And S3Get Policy Attached to it.

First create a cron-job to run for every 5 mins using command

                      sudo crontab -e

And then apply this:-

                      */5 * * * * /usr/bin/S3Refresh.sh

Create a script S3Refresh.sh

The Bash Script is used to fetch the latest folder from the S3 Bucket of Dev Account to a specific Folder in the Linux Machine in the Dev Account.

S3Refresh.sh

                       #!/bin/bash
                       aws s3 sync s3://<bucket_name> local_directory

Change the Mode of the Script as:-

                       chmod +x S3Refresh.sh

Manual Execution Steps Of Bash Script S3Refresh.sh:-

chmod +x S3Refresh.sh
./S3Refresh.sh

BAT File in Windows

Write the Script in a Notepad

                       aws s3 sync <<Path-to-local-folder>> s3://<<s3-bucket-name>>

Save this script as a .bat file as:-

                       Script.bat

For making this script to work as a cron job:-

          1. Go to Task Scheduler
          2. Create a Basic Task
          3. Give a Name to your task
          4. Add triggers for making your bat file to run on certain intervals
          5. Go to Actions:- 1. Provide the path of your .bat file
          6. Save and Start Your Task.
