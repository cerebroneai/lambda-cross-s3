import boto3
import os
import json
from datetime import datetime, timedelta
''' Global Variable defined for Bucket, Object and Filenames '''
SRC_BUCKET_NAME = 'lambdabucket-cerebrone'
DST_BUCKET_NAME = 'mydestination-bucket03'


def download_s3_file(myfile):


s3_source = boto3.client('s3')
items = myfile.split('/')
fparent = items[0]
ffile = items[1]
folder_name = fparent
s3_source.download_file(SRC_BUCKET_NAME, folder_name+'/'+ffile, '/tmp/'+ffile)


def upload_s3_file(myfile):


sts = boto3.client('sts')
sts_result = sts.assume_role(
    RoleArn='arn:aws:iam::984143593165:role/CrossIAM', RoleSessionName='session')
s3_dest = boto3.client('s3', aws_access_key_id=sts_result['Credentials']['accesskeyid'],
                       aws_secret_access_key=sts_result['Credentials']['secretaccesskey'],
                       aws_session_token=sts_result['Credentials']['sessiontoken'])
items = myfile.split('/')
fparent = items[0]
ffile = items[1]
folder_name = fparent
s3_dest.put_object(Bucket=DST_BUCKET_NAME, Key=(folder_name+'/'))
s3_dest.upload_file('/tmp/'+ffile, DST_BUCKET_NAME, folder_name+'/'+ffile)


def lambda_handler(event, context):


try:
print(event)
myfile = event['Records'][0]['s3']['object']['key']
print(myfile)
download_s3_file(myfile)
upload_s3_file(myfile)
except Exception as e:
print(e)
